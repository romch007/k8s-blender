declare module '@ioc:Romch007/Kube' {
  import KubeManager from 'providers/KubeProvider/KubeManager'

  export interface KubeConfig {
    namespace: string
    configFrom: 'file' | 'cluster'
    file?: string
  }
  const kube: KubeManager
  export default kube
}
