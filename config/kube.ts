import { KubeConfig } from '@ioc:Romch007/Kube'

import Env from '@ioc:Adonis/Core/Env'

const kubeConfig: KubeConfig = {
  namespace: Env.get('NAMESPACE'),
  configFrom: Env.get('NODE_ENV') === 'production' ? 'cluster' : 'file',
  file: 'C:/Users/admin/.kube/config-cluster-papa',
}

export default kubeConfig
