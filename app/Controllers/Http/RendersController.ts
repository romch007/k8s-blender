import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import Env from '@ioc:Adonis/Core/Env'
import Kube from '@ioc:Romch007/Kube'

import NewRenderValidator from 'App/Validators/NewRenderValidator'
import Render from 'App/Models/Render'

export default class RendersController {
  public async startRender({ request }: HttpContextContract) {
    const validatedData = await request.validate(NewRenderValidator)

    const render = new Render()
    render.title = validatedData.title
    render.animation = validatedData.animation
    render.startFrame = validatedData.startFrame
    render.endFrame = validatedData.endFrame
    await render.save()

    await validatedData.blend.move(Application.tmpPath('blends'), { name: `${render.id}.blend` })

    if (!render.startFrame || !render.endFrame) {
      return 'error'
    }

    const startFrame = render.animation ? render.startFrame - 1 : 0
    const endFrame = render.animation ? render.endFrame - 1 : 1

    const baseUrl = Env.get('BASE_URL')

    for (let i = startFrame; i < endFrame; i++) {
      const equivFrame = i + 1

      const downloadUrl = `${baseUrl}/get/${render.id}`
      const uploadUrl = `${baseUrl}/uploadFrame`

      Kube.startRenderJob(render.id, equivFrame, downloadUrl, uploadUrl)
    }
  }
}
