FROM node:13-alpine AS builder

WORKDIR /app

RUN apk update && apk add --no-cache git

COPY yarn.lock package.json ./

RUN yarn install --pure-lockfile

COPY . .

RUN node ace build

FROM node:13-alpine

WORKDIR /app

ENV NODE_ENV=production
ENV HOST=0.0.0.0
ENV PORT=80
ENV SESSION_DRIVER=cookie
ENV CACHE_VIEWS=false
ENV DB_CONNECTION=pg

RUN apk update && apk add --no-cache git

COPY yarn.lock package.json ./

RUN yarn install --production --pure-lockfile

COPY --from=builder /app/build .

CMD ["node", "server.js"]
