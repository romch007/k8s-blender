import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Renders extends BaseSchema {
  protected tableName = 'renders'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamps(true)
      table.string('title').notNullable()
      table.boolean('animation').notNullable()
      table.integer('start_frame')
      table.integer('end_frame')
      table.integer('completed_frame').notNullable().defaultTo(0)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
