import * as k8s from '@kubernetes/client-node'

export default class KubeManager {
  private api: k8s.BatchV1Api

  constructor(private namespace: string, private configFile?: string) {
    const kc = new k8s.KubeConfig()
    if (this.configFile) {
      kc.loadFromFile(this.configFile)
    } else {
      kc.loadFromCluster()
    }
    this.api = kc.makeApiClient(k8s.BatchV1Api)
  }

  public startRenderJob(id: number, frame: number, downloadUrl: string, uploadUrl: string) {
    this.api.createNamespacedJob(this.namespace, {
      apiVersion: 'batch/v1',
      kind: 'Job',
      metadata: {
        name: `blender-${id}-${frame}`,
      },
      spec: {
        template: {
          spec: {
            containers: [
              {
                name: 'blender',
                image: 'registry.gitlab.com/romch007/k8s-blender-job:v1',
                env: [
                  { name: 'DOWNLOAD_URL', value: downloadUrl },
                  { name: 'UPLOAD_URL', value: uploadUrl },
                  { name: 'FRAME', value: String(frame) },
                ],
              },
            ],
          },
        },
      },
    })
  }
}
