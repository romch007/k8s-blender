import { ApplicationContract } from '@ioc:Adonis/Core/Application'
import KubeManager from './KubeManager'

/*
|--------------------------------------------------------------------------
| Provider
|--------------------------------------------------------------------------
|
| Your application is not ready when this file is loaded by the framework.
| Hence, the level imports relying on the IoC container will not work.
| You must import them inside the life-cycle methods defined inside
| the provider class.
|
| @example:
|
| public async ready () {
|   const Database = (await import('@ioc:Adonis/Lucid/Database')).default
|   const Event = (await import('@ioc:Adonis/Core/Event')).default
|   Event.on('db:query', Database.prettyPrint)
| }
|
*/
export default class KubeProvider {
  constructor(protected application: ApplicationContract) {}

  public static needsApplication = true

  public register() {
    // Register your own bindings
    this.application.container.singleton('Romch007/Kube', () => {
      const config = this.application.container.use('Adonis/Core/Config').get('kube', {})
      return new KubeManager(config.namespace, config.file)
    })
  }

  public async boot() {
    // All bindings are ready, feel free to use them
  }

  public async ready() {
    // App is ready
  }

  public async shutdown() {
    // Cleanup, since app is going down
  }
}
